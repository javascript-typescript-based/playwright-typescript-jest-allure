import { expect, Locator, Page} from '@playwright/test';

export class HomePage {
  readonly page: Page;
  readonly txtSearch: Locator;
  readonly txtSearchString: string;
  
  constructor(page:Page) {
    this.page = page;
    this.txtSearch = page.locator("//input[@name='q']");  
    this.txtSearchString =  "//input[@name='q']";
  }

  async launchApplication() {
    await this.page.goto('https://google.com');
  }

  async enterSearchText(searchString:string) {
    await this.txtSearch.type(searchString);    
  }

  async verifySearchText(searchText: string){
    expect(await this.page.inputValue(this.txtSearchString)).toEqual(searchText);
  }
  
}