
import {Browser, Page, expect, chromium} from '@playwright/test';
import { HomePage } from '../pages/application/Home.Page';


let browser: Browser;
let page: Page;


fdescribe('First Suite', () =>{
    beforeAll(async () => {
        browser = await chromium.launch({channel:'chrome', headless:false});
        page = await browser.newPage();
      });
      it('Login Feature', async()=> {
      
        await page.goto("https://google.com");
        await page.locator("//input[@name='q']").type("Hello Playwright");
        let actualValue = await page.inputValue("//input[@name='q']");
        let attriValue = await page.locator("//input[@name='q']").getAttribute("name");
        console.log(`Actual Value from the application is: ${actualValue}`)
        console.log(`Attribute Value is ${attriValue}`);
        expect(actualValue).toEqual("Hello Playwright");

    });
})



